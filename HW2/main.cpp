/*******************************************************
 * Homework 2: OpenGL                                  *
 * CS 148 (Summer 2016), Stanford University           *
 *-----------------------------------------------------*
 * First, you should fill in problem1(), problem2(),   *
 * and problem3() as instructed in the written part of *
 * the problem set.  Then, express your creativity     *
 * with problem4()!                                    *
 *                                                     *
 * Note: you will only need to add/modify code where   *
 * it says "TODO".                                     *
 *                                                     *
 * The left mouse button rotates, the right mouse      *
 * button zooms, and the keyboard controls which       *
 * problem to display.                                 *
 *                                                     *
 * For Linux/OS X:                                     *
 * To compile your program, just type "make" at the    *
 * command line.  Typing "make clean" will remove all  *
 * computer-generated files.  Run by typing "./hw2"    *
 *                                                     *
 * For Visual Studio:                                  *
 * You can create a project with this main.cpp and     *
 * build and run the executable as you normally would. *
 *******************************************************/

#include <iostream>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <vector>


#if __APPLE__
    #include <GLUT/glut.h>
#else
    #include <GL/glut.h>
#endif

using namespace std;

bool leftDown = false, rightDown = false;
int lastPos[2];
float cameraPos[4] = {0,1,4,1};
int windowWidth = 640, windowHeight = 480;
double yRot = 0;
int curProblem = 1; // TODO: change this number to try different examples

float specular[] = { 1.0, 1.0, 1.0, 1.0 };
float shininess[] = { 50.0 };

void problem1() {
    // TODO: Your code here!

  for (int i = 0; i < 10; ++i) {
    glPushMatrix();
      glRotatef(i * 36 ,0, 0, 1);
      glTranslatef(1, 0, 0);
      glutSolidTeapot(0.2);
    glPopMatrix();
  }

}

void problem2() {
    // TODO: Your code here!

  double size = 0.24;
  double increment = 0.2;

  for (int i = 0; i < 15; ++i) {
    double scale = i * increment + 1;
    glPushMatrix();
      glTranslatef((size / 2) - ((8 - i) * size), 0, 0);
      double height = scale * size;
      glTranslatef(0, height / 2, 0);
      glScalef(1, scale, 1);
      glutSolidCube(size);
    glPopMatrix();
  }
}

void problem3() {
    // TODO: Your code here!

  double scale = 0.2;
  double horiz_offset = 0.9;
  for (int level = 0; level < 6; ++level) {
    int num_triangles = level + 1;
    double min_offset = - level / 2.0 * horiz_offset;

    for (int i = 0; i < num_triangles; ++i) {
      glPushMatrix();
        glTranslatef(min_offset + (i * horiz_offset), 0, 0);
        glTranslatef(0, (2 - level) * horiz_offset / 2.0 + scale / 2.0, 0);
        glutSolidTeapot(scale);
      glPopMatrix();
    }
  }
}

namespace {

bool ReadFile(string file, vector<string>* lines) {
  ifstream in(file.c_str());
  if (!in) {
    cout << "Could not open file: " << file;
    return false;
  }

  lines->clear();
  string line;
  while (getline(in, line)) {
    lines->push_back(line);
  }
  return true;
}

void SplitString(const string& in, char delim, vector<string>* out) {
  stringstream stream(in);
  out->clear();
  string elem;
  while (getline(stream, elem, delim)) {
    if (!elem.empty()) {
      out->push_back(elem);
    }
  }
}

void RenderModel(string model_file) {
  vector<string> lines;
  assert(ReadFile(model_file, &lines));

  vector<float> vertices;
  vector<float> normals;

  vector<int> v_indices;
  vector<int> n_indices;
  map<int, int> n_to_v;
  for (int s = 0; s < lines.size(); ++s) {
    const string& line = lines.at(s);

    string prefix = line.substr(0, 2);

    if (prefix == "v ") {
      vector<string> parts;
      SplitString(line, ' ', &parts);

      // Only support version that doesn't give 4th coordinate
      if (parts.size() == 4) {
        vertices.push_back(atof(parts.at(1).c_str()));
        vertices.push_back(atof(parts.at(2).c_str()));
        vertices.push_back(atof(parts.at(3).c_str()));
      }
    } else if (prefix == "vn") {
      vector<string> parts;
      SplitString(line, ' ', &parts);

      if (parts.size() == 4) {
        normals.push_back(atof(parts.at(1).c_str()));
        normals.push_back(atof(parts.at(2).c_str()));
        normals.push_back(atof(parts.at(3).c_str()));
      }
    } else if (prefix == "f ") {
      vector<string> parts;
      SplitString(line, ' ', &parts);

      if (parts.size() == 4) {
        for (int i = 1; i < parts.size(); ++i) {
          vector<string> components;
          SplitString(parts.at(i), '/', &components);

          if (components.size() == 1) {
            v_indices.push_back(atoi(components.at(0).c_str()) - 1);
          } else if (components.size() == 2) {
            v_indices.push_back(atoi(components.at(0).c_str()) - 1);
          } else if (components.size() == 3) {
            int v_i = atoi(components.at(0).c_str()) - 1;
            int n_i = atoi(components.at(2).c_str()) - 1;

            v_indices.push_back(v_i);
            n_indices.push_back(n_i);

            n_to_v[n_i] = v_i;
          }
        }
      }
    }
  }

  if (normals.size() > vertices.size()) {
    vector<float> new_vertices;
    new_vertices.resize(normals.size());

    for (map<int, int>::const_iterator it = n_to_v.begin();
         it != n_to_v.end();
         ++it) {

      new_vertices[it->first * 3] = vertices.at(it->second * 3);
      new_vertices[it->first * 3 + 1] = vertices.at(it->second * 3 + 1);
      new_vertices[it->first * 3 + 2] = vertices.at(it->second * 3 + 2);
    }

    v_indices = n_indices;
    vertices = new_vertices;
  }

  cout << v_indices.size();

  bool use_normals = true;
  if (normals.size() != vertices.size()) {
    use_normals = false;
  }

  if (n_indices.size() > 0)
    assert(v_indices.size() == n_indices.size());

  glEnableClientState(GL_VERTEX_ARRAY);

  if (use_normals) {
    glEnableClientState(GL_NORMAL_ARRAY);
    glNormalPointer(GL_FLOAT, 0, &(normals)[0]);
  }

  glVertexPointer(3, GL_FLOAT, 0, &(vertices)[0]);
  glDrawElements(GL_TRIANGLES, v_indices.size(), GL_UNSIGNED_INT, &(v_indices)[0]);

  if (use_normals) {
    glDisableClientState(GL_NORMAL_ARRAY);
  }
  glDisableClientState(GL_VERTEX_ARRAY);
}

void drawPlatform() {

  float vertices[] = {
    1, 0, 1,
    1, 0, -1,
    -1, 0, 1,
    -1, 0, -1
  };

  float normals[] = {
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0
  };

  int indices[] = {
    0, 1, 2,
    2, 1, 3
  };

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);

  glNormalPointer(GL_FLOAT, 0, &(normals)[0]);
  glVertexPointer(3, GL_FLOAT, 0, &(vertices)[0]);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &(indices)[0]);


  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_NORMAL_ARRAY);
}


void drawYoshi() {

  glPushMatrix();
    glTranslatef(0, 0.5, 0);

    glPushMatrix();
      gluSphere(gluNewQuadric(), 0.5, 50, 10);

      // Begin Draw Cheeks
      glPushMatrix();
        glTranslatef(0.2, 0.1, -0.5);
        gluSphere(gluNewQuadric(), 0.4, 50, 10);
      glPopMatrix();
      glPushMatrix();
        glTranslatef(-0.2, 0.1, -0.5);
        gluSphere(gluNewQuadric(), 0.4, 50, 10);
      glPopMatrix();
      // End Draw Cheeks

      // Begin Draw Eyes
      glPushMatrix();
        glTranslatef(0, 0.6, -0.4);
        glScalef(1.2, 1.2, 1.2);
        glPushMatrix();
          glTranslatef(-0.15, 0, 0);
          glPushMatrix();
            glScalef(1, 1.5, 1);
            gluSphere(gluNewQuadric(), 0.2, 50, 10);
          glPopMatrix();

          glPushMatrix();
            glTranslatef(0, -0.07, 0.05);
            glScalef(1, 1.5, 1);
            gluSphere(gluNewQuadric(), 0.2, 50, 10);
          glPopMatrix();

          glPushMatrix();
            glTranslatef(0.04, -0.07, 0.2);
            glScalef(1, 2, 1);
            gluSphere(gluNewQuadric(), 0.07, 50, 10);
          glPopMatrix();
        glPopMatrix();

        glPushMatrix();
          glTranslatef(0.15, 0, 0);
          glPushMatrix();
            glScalef(1, 1.5, 1);
            gluSphere(gluNewQuadric(), 0.2, 50, 10);
          glPopMatrix();

          glPushMatrix();
            glTranslatef(0, -0.07, 0.05);
            glScalef(1, 1.5, 1);
            gluSphere(gluNewQuadric(), 0.2, 50, 10);
          glPopMatrix();

          glPushMatrix();
            glTranslatef(0.04, -0.07, 0.2);
            glScalef(1, 2, 1);
            gluSphere(gluNewQuadric(), 0.07, 50, 10);
          glPopMatrix();
        glPopMatrix();
      glPopMatrix();
      // End Draw Eyes

    glPopMatrix();
  glPopMatrix();
}

// Pikachu model found from:
// https://www.yobi3d.com/v/DNOWwKsCA2/PIKACHU.obj
// I do not take credit for the modeling efforts.
string PIKACHU = "Pikachu.obj";

// Dragon model found from:
// http://tf3dm.com/3d-model/dragon-70267.html
// I do not take credit for the modeling efforts.
string DRAGON = "dragon.obj";
} // namespace

float cameraPosProbFour[4] = {0, -5, 0, 1};
void problem4() {
    // TODO: Your code here!

  // Example of complex transformation
  glPushMatrix();
    glRotatef(20 ,1, 0, 0);
    glTranslatef(0, 0, -0.5);
    glScalef(1, 1, 1);
    drawYoshi();
  glPopMatrix();

  glPushMatrix();
    glScalef(2, 1, 1);
    drawPlatform();
  glPopMatrix();

  glPushMatrix();
    glTranslatef(1, 0, 0);
    glRotatef(270 ,0, 1, 0);
    glScalef(0.1, 0.1, 0.1);
    RenderModel(PIKACHU);
  glPopMatrix();


  glPushMatrix();
    glTranslatef(-1, 0, 0);
    glRotatef(270 ,1, 0, 0);
    glScalef(0.05, 0.05, 0.05);
    RenderModel(DRAGON);
  glPopMatrix();

}

void display() {
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glBegin(GL_LINES);
		glColor3f(1,0,0); glVertex3f(0,0,0); glVertex3f(1,0,0); // x axis
		glColor3f(0,1,0); glVertex3f(0,0,0); glVertex3f(0,1,0); // y axis
		glColor3f(0,0,1); glVertex3f(0,0,0); glVertex3f(0,0,1); // z axis
	glEnd(/*GL_LINES*/);

	glEnable(GL_LIGHTING);
	glShadeModel(GL_SMOOTH);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	glEnable(GL_LIGHT0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0,0,windowWidth,windowHeight);

	float ratio = (float)windowWidth / (float)windowHeight;
	gluPerspective(50, ratio, 1, 1000);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(cameraPos[0], cameraPos[1], cameraPos[2], 0, 0, 0, 0, 1, 0);

  if (curProblem != 4) {
	  glDisable(GL_LIGHT1);
  } else {
	  glEnable(GL_LIGHT1);
	  glLightfv(GL_LIGHT1, GL_POSITION, cameraPosProbFour);
  }

	glRotatef(yRot,0,1,0);

	if (curProblem == 1) problem1();
	if (curProblem == 2) problem2();
	if (curProblem == 3) problem3();
	if (curProblem == 4) problem4();

	glutSwapBuffers();
}

void mouse(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) leftDown = (state == GLUT_DOWN);
	else if (button == GLUT_RIGHT_BUTTON) rightDown = (state == GLUT_DOWN);

	lastPos[0] = x;
	lastPos[1] = y;
}

void mouseMoved(int x, int y) {
	if (leftDown) yRot += (x - lastPos[0])*.1;
	if (rightDown) {
		for (int i = 0; i < 3; i++)
			cameraPos[i] *= pow(1.1,(y-lastPos[1])*.1);
	}


	lastPos[0] = x;
	lastPos[1] = y;
	glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
	curProblem = key-'0';
    if (key == 'q' || key == 'Q' || key == 27){
        exit(0);
    }
	glutPostRedisplay();
}

void reshape(int width, int height) {
	windowWidth = width;
	windowHeight = height;
	glutPostRedisplay();
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("HW2");

	glutDisplayFunc(display);
	glutMotionFunc(mouseMoved);
	glutMouseFunc(mouse);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);

	glutMainLoop();

	return 0;
}
