#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.h"

namespace Raytracer148 {
class Triangle : public Shape {
public:
    // Specify points in clockwise
    Triangle(Eigen::Vector3d& point1, Eigen::Vector3d& point2, Eigen::Vector3d& point3, const Material& mat)
      : p1(point1), p2(point2), p3(point3), material_(mat) {}

    virtual HitRecord intersect(const Ray &ray);

private:
    Eigen::Vector3d p1;
    Eigen::Vector3d p2;
    Eigen::Vector3d p3;

    Material material_;
};
}

#endif
