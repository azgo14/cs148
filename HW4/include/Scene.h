#ifndef SCENE_H
#define SCENE_H

#include "Shape.h"
#include "Image.h"
#include <vector>
#include <limits>

namespace Raytracer148 {
class Scene {
public:
  Scene() {
    lightPoses.resize(2);
    lightColors.resize(2);

    lightPoses[0][0] = lightPoses[0][1] = 3;
    lightPoses[0][2] = 0;

    lightPoses[1][0] = -3;
    lightPoses[1][1] = 3;
    lightPoses[1][2] = 0;


    lightColors[0][0] = lightColors[0][1] = lightColors[0][2] = 1;
    lightColors[1][0] = lightColors[1][1] = lightColors[1][2] = 1;
  }

  virtual ~Scene() {
    for (unsigned int i = 0; i < shapes.size(); i++)
      delete shapes[i];
    shapes.resize(0);
  }

  void addShape(Shape *s) { shapes.push_back(s); }
  HitRecord closestHit(const Ray &ray);
  Eigen::Vector3d trace(const Ray &ray, int recursive_depth);
  void render(Image &image);

private:
  std::vector<Shape*> shapes;
  std::vector<Eigen::Vector3d> lightPoses;
  std::vector<Eigen::Vector3d> lightColors;
};
}

#endif
