#ifndef SHAPE_H
#define SHAPE_H

#include <Eigen/Dense>

using Eigen::Vector3d;

namespace Raytracer148 {
struct Ray {
  Vector3d origin, direction;
};

class Shape;

struct Material {
  int shininess;
  double k_ambient;
  double k_diffuse;
  double k_specular;
  Vector3d color;
  double reflectivity = 0;
};

struct HitRecord {
  Vector3d position, normal;

  double t;

  Material material;
};


class Shape {
public:
  virtual ~Shape(){}
  virtual HitRecord intersect(const Ray &ray) = 0;

private:
  int shininess_;
  double k_ambient_;
  double k_diffuse_;
  double k_specular_;
  Vector3d color_;
};

}

#endif
