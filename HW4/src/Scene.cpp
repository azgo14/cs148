#include "Scene.h"
#include <cmath>
#include <iostream>
#include <random>
using namespace Raytracer148;
using namespace std;
using namespace Eigen;

HitRecord Scene::closestHit(const Ray &ray) {
    HitRecord result;
    result.t = -1;
    bool foundSomething = false;

    for (unsigned int i = 0; i < shapes.size(); i++) {
        HitRecord r = shapes[i]->intersect(ray);
        if (r.t > std::numeric_limits<double>::epsilon() && (!foundSomething || r.t < result.t)) {
            result = r;
            foundSomething = true;
        }
    }

    return result;
}

const double kShadowDelta = 0.01;
const double kShadowMulti = 8;
const double kShadowEps = 0.001;
const double kMaxRecursion = 5;

// Returns an RGB color, where R/G/B are each in the range [0,1]
Vector3d Scene::trace(const Ray &ray, int recursive_depth) {
    // For now we'll have diffuse shading with no shadows, so it's easy!
    HitRecord r = closestHit(ray);

    Vector3d result;
    result[0] = result[1] = result[2] = 0;

    if (r.t < numeric_limits<double>::epsilon()) return result;

    random_device rand;
    mt19937 gen(rand());
    uniform_real_distribution<> distr(-kShadowDelta, kShadowDelta);

    Vector3d diffusion;
    diffusion[0] = diffusion[1] = diffusion[2] = 0;
    Vector3d spectral;
    spectral[0] = spectral[1] = spectral[2] = 0;
    Vector3d ambient;
    ambient[0] = ambient[1] = ambient[2] = 0;

    Vector3d V = - ray.direction;

    for (int l = 0; l < lightPoses.size(); ++l) {
      const Vector3d& lightPos = lightPoses.at(l);
      Vector3d lightColor = lightColors.at(l);

      Vector3d L = (lightPos - r.position).normalized();
      double light_t = (lightPos - r.position).norm();

      lightColor = lightColor / pow(light_t / 4, 2.0);

      // trace shadow
      int shadow_count = 0;

      for (int i = 0; i < kShadowMulti; ++i) {
        Ray shadow_ray;
        shadow_ray.origin = r.position + kShadowEps * r.normal;
        shadow_ray.direction = L;
        shadow_ray.direction[0] += distr(gen);
        shadow_ray.direction[1] += distr(gen);
        shadow_ray.direction[2] += distr(gen);
        HitRecord shadow_r = closestHit(shadow_ray);


        if (shadow_r.t > numeric_limits<double>::epsilon() &&
            shadow_r.t < light_t) {
          shadow_count += 1;
        }
      }

      double shadow_multipler = 1 - shadow_count / kShadowMulti;

      Vector3d R = (2 * L.dot(r.normal) * r.normal - L).normalized();

      diffusion += shadow_multipler * lightColor * max(r.normal.dot(L), 0.0);
      spectral += shadow_multipler * lightColor * pow(max(R.dot(V), 0.0), r.material.shininess);
      ambient += shadow_multipler * lightColor;
    }

    ambient = r.material.k_ambient * r.material.color.cwiseProduct(ambient);
    diffusion = r.material.k_diffuse * r.material.color.cwiseProduct(diffusion);
    spectral = r.material.k_specular * r.material.color.cwiseProduct(spectral);

    result = ambient + diffusion + spectral;

    if (r.material.reflectivity > std::numeric_limits<double>::epsilon() &&
        recursive_depth < kMaxRecursion) {

      Ray reflect_ray;
      reflect_ray.origin = r.position + kShadowEps * r.normal;
      reflect_ray.direction = (2 * V.dot(r.normal) * r.normal - V).normalized();

      result += r.material.reflectivity * trace(reflect_ray, recursive_depth + 1);

    }


    return result;
  }

  const int kGridsPerPixel = 4;
  void Scene::render(Image &image) {
      // We make the assumption that the camera is located at (0,0,0)
      // and that the image plane happens in the square from (-1,-1,1)
      // to (1,1,1).

      double delta = 1. / kGridsPerPixel;

      random_device rand;
      mt19937 gen(rand());
      uniform_real_distribution<> distr(0, delta);

    assert(image.getWidth() == image.getHeight());

    int size = image.getWidth();
    double pixelSize = 2. / size;
    for (int x = 0; x < size; x++)
        for (int y = 0; y < size; y++) {
            Vector3d color;
            color[0] = color[1] = color[2] = 0;

            for (int i = 0; i < kGridsPerPixel; ++i) {
              double x_hat = x + i * delta + distr(gen);

              for (int j = 0; j < kGridsPerPixel; ++j) {
                double y_hat = y + j * delta + distr(gen);

                Ray curRay;
                curRay.origin[0] = curRay.origin[1] = curRay.origin[2] = 0;
                curRay.direction[0] = (x_hat + 0.5) * pixelSize - 1;
                curRay.direction[1] = 1 - (y_hat + 0.5) * pixelSize;
                curRay.direction[2] = 1;
                curRay.direction.normalize();

                color += trace(curRay, 0);
              }
            }

            color = color / kGridsPerPixel / kGridsPerPixel;

            image.setColor(x, y, color[0], color[1], color[2]);
        }
}
