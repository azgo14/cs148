#include <iostream>
#include "Image.h"
#include "Scene.h"
#include "Sphere.h"
#include "Triangle.h"
using namespace std;
using namespace Raytracer148;
using namespace Eigen;

int main() {
    Image im(700, 700);

    Scene scene;

    Material material;
    material.color[0] = 0.3;
    material.color[1] = 0;
    material.color[2] = 0;
    material.k_ambient = 0.1;
    material.k_diffuse = 2;
    material.k_specular = 1;
    material.shininess = 8;
    material.reflectivity = 0.8;


    Vector3d center;
    center[0] = 0;
    center[1] = 0;
    center[2] = 4.5;
    scene.addShape(new Sphere(center, 2, material));

    material.color[0] = 0;
    material.color[1] = .3;
    material.color[2] = 0;
    material.reflectivity = 0.0;

    center[0] = -1.5;
    center[1] = 2;
    center[2] = 3.5;
    scene.addShape(new Sphere(center, .5, material));

    material.color[0] = 0;
    material.color[1] = .3;
    material.color[2] = .3;
    material.reflectivity = 0.7;

    center[0] = 1.5;
    center[1] = 2.25;
    center[2] = 4.25;
    scene.addShape(new Sphere(center, .5, material));

    material.color[0] = 0.4;
    material.color[1] = 0.3;
    material.color[2] = 0;
    material.reflectivity = 0.0;

    Vector3d p1;
    p1[0] = -50;
    p1[1] = -2;
    p1[2] = 0;

    Vector3d p2;
    p2[0] = 50;
    p2[1] = -2;
    p2[2] = 50;

    Vector3d p3;
    p3[0] = 50;
    p3[1] = -2;
    p3[2] = 0;
    scene.addShape(new Triangle(p1, p2, p3, material));

    p1[0] = -50;
    p1[1] = -2;
    p1[2] = 0;

    p2[0] = -50;
    p2[1] = -2;
    p2[2] = 50;

    p3[0] = 50;
    p3[1] = -2;
    p3[2] = 50;
    scene.addShape(new Triangle(p1, p2, p3, material));

    scene.render(im);

    im.writePNG("test.png");

    return 0;
}
