#include "Triangle.h"

#include <iostream>
#include <limits>
using namespace Raytracer148;
using namespace Eigen;
using namespace std;

HitRecord Triangle::intersect(const Ray &ray) {
    HitRecord result;
    result.t = -1;

    Eigen::Vector3d normal = (p2 - p1).cross(p3 - p1).normalized();

    // Plane intersection first per http://inst.eecs.berkeley.edu/~cs184/fa12/slides/lecture17.pdf

    double t = (p1.dot(normal) - ray.origin.dot(normal)) / ray.direction.dot(normal);

    if (t < numeric_limits<double>::epsilon()) return result; // no hit

    Eigen::Vector3d hit = ray.direction * t + ray.origin;

    if ((p2 - p1).cross(hit - p1).dot(normal) > numeric_limits<double>::epsilon() &&
        (p3 - p2).cross(hit - p2).dot(normal) > numeric_limits<double>::epsilon() &&
        (p1 - p3).cross(hit - p3).dot(normal) > numeric_limits<double>::epsilon()) {

      result.t = t;
      result.position = hit;
      result.normal = normal;
    }

    result.material = material_;

    return result;
}
