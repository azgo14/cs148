// Credit to http://dev.miaumiau.cat/sph/ for guiding how we structure our program

// Key, vertName, fragName
let shaderNames = {
  "draw3DDataToTexPosition": ["vert_draw3DDataToTex", "frag_draw3DDataToTex"],
  "draw3DDataToTexVelocity": ["vert_draw3DDataToTex", "frag_draw3DDataToTex"],
  "renderParticle": ["vert_renderParticle", "frag_renderParticle"],
  "updatePosition": ["vert_updatePosition", "frag_updatePosition"],
  "updateDensity": ["vert_updateDensity", "frag_updateDensity"],
  "updateMedianDensity": ["vert_median", "frag_median4"],
  "updateAcceleration": ["vert_acceleration", "frag_acceleration"],
  "mvCopyToOrigPosition" : ["vert_cpTex", "frag_cpTex"],
  "median": ["vert_median", "frag_median"],
}

let shaders = {}

function initShaders() {
  for (let key of Object.keys(shaderNames)) {
    let vertName = shaderNames[key][0];
    let fragName = shaderNames[key][1];

    let vertShadSource = document.getElementById(vertName).text;
    let vertShad = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertShad, vertShadSource);
    gl.compileShader(vertShad);


    let fragShadSource = document.getElementById(fragName).text;
    let fragShad = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragShad, fragShadSource);
    gl.compileShader(fragShad);

    let program = gl.createProgram();
    gl.attachShader(program, vertShad);
    gl.attachShader(program, fragShad);
    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
      alert("Can't load shader! " + vertName + " " + fragName);
      console.log("Vert Errors");
      console.log(gl.getShaderInfoLog(vertShad));
      console.log("Frag Errors");
      console.log(gl.getShaderInfoLog(fragShad));
    }

    shaders[key] = program;
  }

  for (let key of Object.keys(shaderNames)) {
    if (key == "renderParticle") {
      console.log("Setting up renderParticle shader params");
      shaders[key].index2D = gl.getAttribLocation(shaders[key], "index2D");
      gl.enableVertexAttribArray(shaders[key].index2D);

      shaders[key].modelviewUniform = gl.getUniformLocation(shaders[key], "modelview");
      shaders[key].projectionUniform = gl.getUniformLocation(shaders[key], "projection");
      shaders[key].positionT = gl.getUniformLocation(shaders[key], "positionT");

    } else if (key == "draw3DDataToTexPosition") {
      console.log("Setting up draw3DDataToTexPosition shader params");
      shaders[key].data3D = gl.getAttribLocation(shaders[key], "data3D");
      gl.enableVertexAttribArray(shaders[key].data3D);

      shaders[key].index2D = gl.getAttribLocation(shaders[key], "index2D");
      gl.enableVertexAttribArray(shaders[key].index2D);

    } else if (key == "draw3DDataToTexVelocity") {
      console.log("Setting up draw3DDataToTexVelocity shader params");
      shaders[key].data3D = gl.getAttribLocation(shaders[key], "data3D");
      gl.enableVertexAttribArray(shaders[key].data3D);

      shaders[key].index2D = gl.getAttribLocation(shaders[key], "index2D");
      gl.enableVertexAttribArray(shaders[key].index2D);

    } else if (key == "updatePosition") {
      console.log("Setting up updatePosition shader params");
      shaders[key].index2D = gl.getAttribLocation(shaders[key], "index2D");
      gl.enableVertexAttribArray(shaders[key].index2D);

      shaders[key].positionT = gl.getUniformLocation(shaders[key], "positionT");
      shaders[key].velocityT = gl.getUniformLocation(shaders[key], "velocityT");

    } else if (key == "updateDensity") {
      console.log("Setting up updateDensity shader params");
      shaders[key].index2D = gl.getAttribLocation(shaders[key], "index2D");
      gl.enableVertexAttribArray(shaders[key].index2D);

      shaders[key].medianPositionT = gl.getUniformLocation(shaders[key], "medianPositionT");
      shaders[key].positionT = gl.getUniformLocation(shaders[key], "positionT");
      shaders[key].adjecentVoxels = gl.getUniformLocation(shaders[key], "adjecentVoxels");

    } else if (key == "median") {
      shaders[key].vertex2DIndex = gl.getAttribLocation(shaders[key], "positionIndex");
      gl.enableVertexAttribArray(shaders[key].vertex2DIndex);
      shaders[key].positionTexture = gl.getUniformLocation(shaders[key], "positionTexture");
      shaders[key].dataTexture = gl.getUniformLocation(shaders[key], "dataTexture");
      shaders[key].particleSize = gl.getUniformLocation(shaders[key], "particleSize");
      shaders[key].alpha = gl.getUniformLocation(shaders[key], "alpha");

    } else if (key == "updateMedianDensity") {
      shaders[key].vertex2DIndex = gl.getAttribLocation(shaders[key], "positionIndex");
      gl.enableVertexAttribArray(shaders[key].vertex2DIndex);
      shaders[key].positionTexture = gl.getUniformLocation(shaders[key], "positionTexture");
      shaders[key].densityTexture = gl.getUniformLocation(shaders[key], "dataTexture");
      shaders[key].particleSize = gl.getUniformLocation(shaders[key], "particleSize");

    } else if (key == "updateVelocity") {
      // not implemented

    } else if (key == "mvCopyToOrigPosition") {
      console.log("Setting up mvCopyToOrigPosition shader params");

      shaders[key].index2D = gl.getAttribLocation(shaders[key], "index2D");
      gl.enableVertexAttribArray(shaders[key].index2D);

      shaders[key].texture = gl.getUniformLocation(shaders[key], "texture");
    } else {
      alert("Could not initialize shader name: " + name);
    }
  }
}

// NOTE: This function will bind the buffers that the shader expects!
function useShader(name) {
  gl.useProgram(shaders[name]);

  if (name == "renderParticle") {
    gl.bindBuffer(gl.ARRAY_BUFFER, textureIndexBuf);
    gl.vertexAttribPointer(shaders[name].index2D, 2, gl.FLOAT, false, 0, 0);

    gl.uniformMatrix4fv(shaders[name].projectionUniform, false, projection);
    gl.uniformMatrix4fv(shaders[name].modelviewUniform, false, modelview);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, positionT);
    gl.uniform1i(shaders[name].positionT, 0);

  } else if (name == "draw3DDataToTexPosition") {
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuf);
    gl.vertexAttribPointer(shaders[name].data3D, 3, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, textureIndexBuf);
    gl.vertexAttribPointer(shaders[name].index2D, 2, gl.FLOAT, false, 0, 0);

  } else if (name == "draw3DDataToTexVelocity") {
    gl.bindBuffer(gl.ARRAY_BUFFER, velocityBuf);
    gl.vertexAttribPointer(shaders[name].data3D, 3, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, textureIndexBuf);
    gl.vertexAttribPointer(shaders[name].index2D, 2, gl.FLOAT, false, 0, 0);

  } else if (name == "updatePosition") {
    gl.bindBuffer(gl.ARRAY_BUFFER, textureIndexBuf);
    gl.vertexAttribPointer(shaders[name].index2D, 2, gl.FLOAT, false, 0, 0);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, positionT);
    gl.uniform1i(shaders[name].positionT, 0);
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, velocityT);
    gl.uniform1i(shaders[name].velocityT, 1);

  } else if (name == "updateDensity") {
    gl.bindBuffer(gl.ARRAY_BUFFER, textureIndexBuf);
    gl.vertexAttribPointer(shaders[name].index2D, 2, gl.FLOAT, false, 0, 0);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, positionT);
    gl.uniform1i(shaders[name].positionT, 0);
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, voxelsT);
    gl.uniform1i(shaders[name].medianPositionT, 1);

    //bindTexture(shader[n].medianPositionTexture, tCells1, 1);

  } else if (name == "updateVelocity") {


  } else if (name == "updateMedianDensity") {
    gl.bindBuffer(gl.ARRAY_BUFFER, index2DBuf);
    gl.vertexAttribPointer(shaders[name].vertex2DIndex, 2, gl.FLOAT, false, 0, 0);
    gl.uniform1f(shaders[name].particleSize, 1);
    bindTexture(shaders[name].positionTexture, positionTexture, 0);
    bindTexture(shaders[name].densityTexture, tVelDen, 1);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, positionT);
    gl.uniform1i(shaders['median'].positionTexture, 0);
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(shaders['median'].densityTexture, 1);

  } else if (name == "mvCopyToOrigPosition") {
    console.log("Setting up mvCopyToOrigPosition shader params");

    gl.bindBuffer(gl.ARRAY_BUFFER, textureIndexBuf);
    gl.vertexAttribPointer(shaders[name].index2D, 2, gl.FLOAT, false, 0, 0);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, positionCopyT);
    gl.uniform1i(shaders[name].texture, 0);
  } else {
    alert("Unsupported name: " + name);
  }
}

function initTexture(width, height, format, type, min_interp, max_interp) {
  let texture = gl.createTexture();
  texture.width = width;
  texture.height = height;
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, min_interp);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, max_interp);
  gl.texImage2D(gl.TEXTURE_2D, 0, format, width, height, 0, format, type, null);
  gl.bindTexture(gl.TEXTURE_2D, null);

  return texture;
}

function createFloat32ArrayBuffer(data) {
  let buf = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buf);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW);
  return buf;
}

function median(buffer, texture, numVertex, size, alpha) {
    gl.bindFramebuffer(gl.FRAMEBUFFER, buffer);
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.viewport(0, 0, size, size);
    gl.enable(gl.BLEND);
    gl.blendEquation(gl.FUNC_ADD);
    gl.blendFuncSeparate(gl.ONE, gl.ONE, gl.ONE, gl.ONE);

    gl.useProgram(shaders['median']);
    gl.bindBuffer(gl.ARRAY_BUFFER, index2DBuf);
    gl.vertexAttribPointer(shaders['median'].vertex2DIndex, 2, gl.FLOAT, false, 0, 0);
    gl.uniform1f(shaders['median'].particleSize, 1);
    gl.uniform1f(shaders['median'].alpha, alpha);

    gl.bindTexture(gl.TEXTURE_2D, positionT);
    gl.uniform1i(shaders['median'].positionTexture, 0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(shaders['median'].dataTexture, 1);

    gl.drawArrays(gl.POINTS, 0, numVertex);
    gl.disable(gl.BLEND);
}

// No depth info stored. Ok?
//
// Does not keep the framebuffer bind
function setTextureToFramebuffer(texture) {
    let frameBuffer = {};
    frameBuffer = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, frameBuffer);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    frameBuffer.width = texture.width;
    frameBuffer.height = texture.height;

    return frameBuffer;
}

// Make sure to find the necessary buffer data beforehand as defined in useShader
function drawPointToFrameBuffer(frameBuffer, primitiveSize, shaderName) {
  gl.bindFramebuffer(gl.FRAMEBUFFER, frameBuffer);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.viewport(0, 0, frameBuffer.width, frameBuffer.height);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  useShader(shaderName);
  gl.drawArrays(gl.POINTS, 0, primitiveSize);
}
