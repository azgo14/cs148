var gl;


var projection = mat4.create();
var modelview = mat4.create();

var viewportWidth = 600;
var viewportHeight = 400;
mat4.perspective(projection , 45, viewportWidth / viewportHeight, 0.1, 10.0);
mat4.translate(modelview, modelview, [0.0, 0.0, -9.0]);

var textureDim = 128;
var numParticles = textureDim * textureDim;


// Buffers
var positionBuf;
var velocityBuf;
var textureIndexBuf;
var index2DBuf;

// Framebuffers
var positionFB;
var velocityFB;

// Textures
var positionT;
var velocityT;

var voxelsT;
var medianPositionT;

// TO DELETE SINCE JUST TESTING
var positionCopyT;
var positionCopyFB;


var dimScale = 3;
var cubeVert;
var positionData;
var velocityData;
var textureIndex;

var adjecentVoxels;

// constants
var gravityConstant;


function main() {
  let canvas = document.getElementById("main");
  gl = canvas.getContext("experimental-webgl");
  gl.getExtension('OES_texture_float');

  canvas.width  = viewportWidth;
  canvas.height = viewportHeight;
  canvas.style.width  = canvas.width + 'px';
  canvas.style.height = canvas.height + 'px';

  gl.viewportWidth = canvas.width;
  gl.viewportHeight = canvas.height;
  initShaders();
  initScene();

  renderFrame();
}

function initScene() {
  cubeVert = [
    -dimScale, -dimScale, -dimScale,
    -dimScale, -dimScale, dimScale,
    -dimScale, dimScale, dimScale,
    dimScale, dimScale, -dimScale,
    -dimScale, -dimScale, -dimScale,
    -dimScale, dimScale, -dimScale,
    dimScale, -dimScale, dimScale,
    -dimScale, -dimScale, -dimScale,
    dimScale, -dimScale, -dimScale,
    dimScale, dimScale, -dimScale,
    dimScale, -dimScale, -dimScale,
    -dimScale, -dimScale, -dimScale,
    -dimScale, -dimScale, -dimScale,
    -dimScale, dimScale, dimScale,
    -dimScale, dimScale, -dimScale,
    dimScale, -dimScale, dimScale,
    -dimScale, -dimScale, dimScale,
    -dimScale, -dimScale, -dimScale,
    -dimScale, dimScale, dimScale,
    -dimScale, -dimScale, dimScale,
    dimScale, -dimScale, dimScale,
    dimScale, dimScale, dimScale,
    dimScale, -dimScale, -dimScale,
    dimScale, dimScale, -dimScale,
    dimScale, -dimScale, -dimScale,
    dimScale, dimScale, dimScale,
    dimScale, -dimScale, dimScale,
    dimScale, dimScale, dimScale,
    -dimScale, dimScale, dimScale,
    dimScale, -dimScale, dimScale
  ];

  positionData = [];
  // Positions are 3D. Randomly to one corner of the cube
  for (let i = 0; i < numParticles; ++i) {
    positionData.push(Math.random() * dimScale);
    positionData.push(Math.random() * dimScale);
    positionData.push(Math.random() * dimScale);
  }

  if (positionData.length / 3 != numParticles) {
    alert("position length and numParticles not the same!");
  }

  velocityData = [];
  // Positions are 3D. Randomly to one corner of the cube
  for (let i = 0; i < numParticles; ++i) {
    velocityData.push(0 + Math.random() * 0.01);
    velocityData.push(0.01 + Math.random() * 0.01);
    velocityData.push(0);
  }

  if (positionData.length != velocityData.length) {
    alert("position length and velocity length are not the same!");
  }

  textureIndex = [];
  // Texture is 2 dimensional and has to be in between values [-1, 1]
  // since output of velocity shader is in the normalized cube space.
  let increment = 1 / textureDim;
  for (let r = 0; r < textureDim; r++) {
    for (let c = 0; c < textureDim; c++) {
      // 0.5 because we want to center the index
      textureIndex.push((r + 0.5) * increment);
      textureIndex.push((c + 0.5) * increment);
    }
  }

  if (textureIndex.length / 2 != positionData.length / 3) {
    alert("index length and position length are not the same!");
  }

  positionBuf = createFloat32ArrayBuffer(positionData);
  velocityBuf = createFloat32ArrayBuffer(velocityData);
  textureIndexBuf = createFloat32ArrayBuffer(textureIndex);

  // TODO(dkislyuk): cleanup
  var indexBufferData = []
  for (i = 0; i < 1048576; i ++) {
      div = 0.0009765625;
      indexBufferData.push(div * ((i % 1024.) + 0.5), div * (Math.floor(i * div) + 0.5));
  }

  index2DBuf = createFloat32ArrayBuffer(indexBufferData)

  voxelsT = initTexture(textureDim, textureDim, gl.RGB, gl.FLOAT, gl.NEAREST, gl.NEAREST);
  positionT = initTexture(textureDim, textureDim, gl.RGB, gl.FLOAT, gl.NEAREST, gl.NEAREST);
  velocityT = initTexture(textureDim, textureDim, gl.RGB, gl.FLOAT, gl.NEAREST, gl.NEAREST);

  // This can be placed with the velocityT texture, but let's keep it clean and
  // separated for now.
  densityT = initTexture(textureDim, textureDim, gl.RGB, gl.FLOAT, gl.NEAREST, gl.NEAREST);

  positionFB = setTextureToFramebuffer(positionT);
  velocityFB = setTextureToFramebuffer(velocityT);
  voxelsFB = setTextureToFramebuffer(voxelsT);
  densityFB = setTextureToFramebuffer(densityT);

  // Vectors to get to neighboring indices
  adjecentVoxels = []

  for (i = -1; i <= 1; i++) {
    for (j = -1; j <= 1; j++) {
      for (k = -1; k <= 1; k++) {
        adjecentVoxels.push((i, j, k));
      }
    }
  }

  /* Setup constants */
  weightDefaultConstant = 0.00000597641;

  // TO DELETE
  positionCopyT = initTexture(textureDim, textureDim, gl.RGB, gl.FLOAT, gl.NEAREST, gl.NEAREST);
  positionCopyFB = setTextureToFramebuffer(positionCopyT);

  drawPointToFrameBuffer(positionFB, numParticles, "draw3DDataToTexPosition");
  drawPointToFrameBuffer(velocityFB, numParticles, "draw3DDataToTexVelocity");
}

function renderFrame() {
  median(voxelsFB.buffer, positionT, numParticles, 1024, 1);

  // drawPointToFrameBuffer(densityFB, numParticles, "updateDensity");
  drawPointToFrameBuffer(densityFB, numParticles, "updateDensity");

  drawPointToFrameBuffer(positionCopyFB, numParticles, "updatePosition");
  drawPointToFrameBuffer(positionFB, numParticles, "mvCopyToOrigPosition");

  gl.bindFramebuffer(gl.FRAMEBUFFER, null);

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  useShader("renderParticle");
  gl.drawArrays(gl.POINTS, 0, numParticles);

  window.requestAnimationFrame(renderFrame);
}


main();
