//out vec4 color;

//in vec3 FragPos;
//in vec3 Normal;

//uniform vec3 lightPos;
//uniform vec3 viewPos;
//uniform vec3 lightColor;
//uniform vec3 objectColor;

// Phong illumination model equation from:
// https://en.wikipedia.org/wiki/Phong_reflection_model
void main()
{
    // Define vectors used by equation
    //vec3 N = normalize(Normal);
    //vec3 L = normalize(lightPos - FragPos);
    //vec3 V = normalize(viewPos - FragPos);
    //vec3 R = normalize(2 * dot(L, N) * N - L);

    //int shininess = 45;
    //float k_ambient = 0.12;
    //float k_diffuse = 1;
    //float k_specular = 0.55;

    //vec3 ambientColor = objectColor * lightColor ;
    //vec3 diffuseColor = objectColor * lightColor;
    //vec3 spectralColor = objectColor * lightColor ;

    //vec3 diffusion = diffuseColor * max(dot(N, L), 0);
    //vec3 spectral = spectralColor * pow(max(dot(R, V), 0), shininess);
    //vec3 ambient = ambientColor;

    //color = vec4(k_ambient * ambient +
      //           k_diffuse * diffusion +
        //         k_specular * spectral, 1);


    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}
